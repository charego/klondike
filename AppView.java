import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class AppView extends JFrame {
	private final Solitaire game;
	private JPanel GP;
	private JButton newGame;
	private List<PanelCell> PL;
	private PanelCell fromPanel;
	private AppViewInformer AVI;

	public AppView(final Solitaire game) {
		this.game = game;
		AVI = new AppViewInformer();
		PL = new ArrayList<PanelCell>();
		PL.add(new PanelBlankCard(AVI));
		PL.add(new PanelDraw(game.getDrawCell(), AVI));
		for (int i = 0; i <= 3; i++)
			PL.add(new PanelHome(game.getHomeCell(i), AVI));
		for (int i = 0; i <= 6; i++)
			PL.add(new PanelTableau(game.getTableau(i), AVI));
		GP = new PanelGame(PL);
		newGame = new JButton("New Game");
		fromPanel = null;

		setContentPane(new PanelFeltBackground());
		Container c = getContentPane();
		c.add(GP);
		c.add(newGame, BorderLayout.SOUTH);

		newGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (fromPanel != null)
					fromPanel.toggleSelected();
				fromPanel = null;
				game.newGame();
				repaint();
			}
		});
	}

	private class AppViewInformer implements ViewInformer {
		private AppViewInformer() {}

		public void panelPressed(PanelCell panel) {
			// BLANK CARD PANEL
			if (panel instanceof PanelBlankCard) {
				game.updateDrawCell();
				PL.get(1).repaint();
				if (fromPanel != null) {
					fromPanel.toggleSelected();
					fromPanel = null;
				}
			}
			// DRAW PANEL
			else if (panel instanceof PanelDraw) {
				if (fromPanel == null) {
					fromPanel = panel;
					fromPanel.toggleSelected();
				} else {
					fromPanel.toggleSelected();
					fromPanel = null;
				}
			}
			// TABLEAU PANEL
			else if (panel instanceof PanelTableau) {
				if (fromPanel == null) {
					fromPanel = panel;
					fromPanel.toggleSelected();
				} else if (fromPanel != panel) {
					if (game.move(fromPanel.getCell(), panel.getCell())) {
						panel.repaint();
						if (game.isWon())
							System.out.println("You Won!!!");
					}
					fromPanel.toggleSelected();
					fromPanel = null;
				}
			}
			// HOME PANEL
			else {
				if (fromPanel == null) {
					fromPanel = panel;
					fromPanel.toggleSelected();
				} else if (fromPanel != panel) {
					if (game.move(fromPanel.getCell(), panel.getCell())) {
						panel.repaint();
						if (game.isWon())
							System.out.println("You Won!!!");
					}
					fromPanel.toggleSelected();
					fromPanel = null;
				}
			}
		}
	}
}