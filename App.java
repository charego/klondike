import javax.swing.JFrame;

/**
 * Application for playing Solitaire.
 * @author Charles Gould
 */
public class App
{
    public static void main(String[] args)
    {
        final JFrame view = new AppView(new Solitaire());
        view.setTitle("Solitaire");
        view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        view.setSize(800, 675);
        view.setResizable(false);
        view.setVisible(true);
    }
}