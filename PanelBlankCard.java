import java.awt.Graphics;
import javax.swing.Icon;

@SuppressWarnings("serial")
public class PanelBlankCard extends PanelCell
{
	public PanelBlankCard(ViewInformer v)
	{
		super(null, v);
	}
	
	public void paintComponent(Graphics g)
	{
    	super.paintComponent(g);
    	Icon image = Card.getBack();
    		int x = (getWidth() - image.getIconWidth()) / 2;
    		int y = (getHeight() - image.getIconHeight()) / 2;
    		image.paintIcon(this, g, x, y);
	}
}