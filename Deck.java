import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck
{
	private ArrayStack<Card> deck;

	public Deck()
	{
		deck = new ArrayStack<Card>();
		for (int i = 1; i <= 13; i++)
		{
			deck.push(new Card(Suit.spade, i));
			deck.push(new Card(Suit.heart, i));
			deck.push(new Card(Suit.diamond, i));
			deck.push(new Card(Suit.club, i));
		}
	}

	public Card deal()
	{
		return deck.pop();
	}

	public void shuffle()
	{
		List<Card> tmplist = new ArrayList<Card>();
		while (! deck.isEmpty())
			tmplist.add(deck.pop());
		Collections.shuffle(tmplist);
		while (! tmplist.isEmpty())
			deck.push(tmplist.remove(0));
	}

	public int size()
	{
        return deck.size();
    }

	public boolean isEmpty()
	{
		return (deck.isEmpty());
	}

	public String toString()
	{
		String deckStr = "Deck (top to bottom):\n";
		for (Card c: deck)
			deckStr += c.toString() + " ";
		return deckStr;
	}
}