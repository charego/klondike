import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.Icon;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PanelCell extends JPanel
{
    protected Cell cell;
    protected PanelCell cellPanel;
    protected boolean isSelected;
    protected static Color highlight;
    protected static int cardWidth, cardHeight;
    protected ViewInformer viewInformer;
    protected MouseListener listener;

    public PanelCell(Cell c, ViewInformer v)
    {
    	setOpaque(false);
        cell = c;
        cellPanel = this;
        isSelected = false;
        highlight = new Color(255, 255, 0, 150);
        Icon image = Card.getBack();
        cardWidth = image.getIconWidth();
        cardHeight = image.getIconHeight();
        viewInformer = v;
        this.addMouseListener(new MouseListener()
        {
        	public void mousePressed(MouseEvent e)
        	{
        		viewInformer.panelPressed(cellPanel);
        	}
			public void mouseClicked(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
        });
    }
    
    public Cell getCell()
    {
    	if (cell != null)
    		return cell;
    	return null;
    }

	public void toggleSelected()
	{
		if (! isSelected)
			isSelected = true;
		else
			isSelected = false;
		repaint();
	}
}
