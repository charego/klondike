import java.util.ArrayList;
import java.util.List;

public class Solitaire
{
	private Deck deck;
	private List<Tableau> tableauList;
	private List<CellHome> homecellList;
	private Cell drawCell;
	
	public Solitaire()
	{	
		tableauList = new ArrayList<Tableau>();
		homecellList = new ArrayList<CellHome>();
		drawCell = new CellDraw();
		for (int i=1; i<=7; i++)
			tableauList.add(new Tableau());
		for (int i=1; i<=4; i++)
			homecellList.add(new CellHome());
		newGame();
	}
	
	public boolean move(Cell fromCell, Cell toCell)
	{
		if (toCell.moveFrom(fromCell))
		{
			if (! fromCell.isEmpty())
			{
				if (! fromCell.getTop().isFaceUp() && ! (fromCell instanceof CellDraw))
					fromCell.getTop().turn();
			}
			return true;
		}
		return false;
	}
	
	public void newGame()
	{
		deck = new Deck();
		deck.shuffle();
		for (Cell c: tableauList)
			c.clear();
		for (Cell c: homecellList)
			c.clear();
		drawCell.clear();
		int indexUpCard = 0;
		int numPanelsToDeal = 1;
		while (numPanelsToDeal < 7)
		{
			Card upCard = deck.deal();
			upCard.turn();
			tableauList.get(indexUpCard).dealCard(upCard);
			for (int i=numPanelsToDeal; i<=6; i++)
			{
				tableauList.get(i).dealCard(deck.deal());
			}
			indexUpCard++;
			numPanelsToDeal++;
		}
		Card upCard = deck.deal();
		upCard.turn();
		tableauList.get(6).dealCard(upCard);
		while (! deck.isEmpty()) {
			Card card = deck.deal();
			card.turn();
			drawCell.addCard(card);
		}
	}
	
	public Cell getTableau(int i)
	{
		return tableauList.get(i);
	}
	
	public Cell getHomeCell(int i)
	{
		return homecellList.get(i);
	}
	
	public Cell getDrawCell()
	{
		return drawCell;
	}
	
	public void updateDrawCell()
	{
		((CellDraw)drawCell).updateDisplay();
	}
	
	public boolean isWon()
	{
		for (int i=0; i<=3; i++)
			if (!(getHomeCell(i).size() == 13))
				return false;
		return true;
	}
	
	public String toString()
	{
		String ret = "Drawing Pile:\n";
		for (Card c: drawCell)
			ret += c.toString() + " ";
		ret += "\n\n";
		for (int i=0; i<=6; i++)
			ret += "T" + (i+1) + " " +
			getTableau(i).toString() + "\n";
		return ret;
	}
}
