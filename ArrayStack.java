import java.util.AbstractCollection;import java.util.Iterator;import java.util.List;import java.util.ArrayList;public class ArrayStack<E> extends AbstractCollection<E>{    private List<E> list = new ArrayList<E>();    public ArrayStack() {}        public String toString()    {    	String rep = "";    	for (E elem: list)    		rep = elem + "| " + rep;    	return rep;    }        public E peek()    {        return list.get(list.size() - 1);    }    public E pop()    {        return list.remove(list.size() - 1);    }    public void push(E newElement)    {        list.add(newElement);    }

    public int size()    {
        return list.size();
    }

    public boolean add(E newElement)    {
        this.push(newElement);
        return true;
    }

    public Iterator<E> iterator()    {
        return new StackIterator<E>(list.iterator());
    }    	@SuppressWarnings("hiding")	private class StackIterator<E> implements Iterator<E>	{		private Iterator<E> iter;				private StackIterator(Iterator<E> iter)		{			this.iter = iter;		}		public boolean hasNext()		{			return iter.hasNext();		}		public E next()		{			return iter.next();		}		public void remove()		{			throw new UnsupportedOperationException("remove not supported by Stack");		}    }}