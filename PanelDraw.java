import java.awt.Graphics;
import javax.swing.Icon;

@SuppressWarnings("serial")
public class PanelDraw extends PanelCell
{
	public PanelDraw(Cell c, ViewInformer v)
	{
		super(c, v);
	}

    public void paintComponent(Graphics g)
    {
    	super.paintComponent(g);
    	if (! cell.isEmpty())
    	{
    		Icon image = cell.getTop().getImage();
			int y = (getHeight() - image.getIconHeight()) / 2;
			image.paintIcon(this, g, 0, y);
        	if (isSelected)
        	{
        		g.setColor(highlight);
    			g.fillRect(0, y, cardWidth, cardHeight);
        	}
    	}
    }
}