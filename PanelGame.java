import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PanelGame extends JPanel
{	
	private GridBagLayout gridbag;
	private GridBagConstraints gbc;
	
	public PanelGame(List<PanelCell> panelList)
	{
		gridbag = new GridBagLayout();
    	gbc = new GridBagConstraints();
    	setLayout(gridbag);
    	setOpaque(false);
    	gbc.gridx=0;
    	gbc.gridy=0;
    	gbc.weightx=1;
    	gbc.weighty=1;
    	gbc.fill=GridBagConstraints.BOTH;
    	add(panelList.get(0), gbc);
    	gbc.gridx=1;
    	gbc.gridwidth = 2;
    	add(panelList.get(1), gbc);
    	gbc.gridwidth = 1;
    	for (int i=2; i<=5; i++)
    	{
    		gbc.gridx=(i+1);
    		add(panelList.get(i), gbc);
    	}
    	gbc.gridx=0;
    	gbc.gridy=1;
    	gbc.weighty=4;
    	add(panelList.get(6), gbc);
    	gbc.gridx=GridBagConstraints.RELATIVE;
    	for (int i=7; i<=12; i++)
    		add(panelList.get(i), gbc);
	}
}
