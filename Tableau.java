import java.util.LinkedList;

public class Tableau extends CellAbstract
{
	private LinkedList<Integer> orderList;
	
	public Tableau()
	{
		orderList = new LinkedList<Integer>();
	}

	public void dealCard(Card card)
	{
		if (isEmpty())
			orderList.add(1);
		else
		{
			int i;
			if (card.getRank() == getTop().getRank()-1 &&
				! card.getColor().equals(getTop().getColor()))
			{
				i = orderList.removeLast();
				i++; orderList.add(i);
			}
			else
				orderList.add(1);
		}
		list.add(card);
	}
	
	public void addCard(Card card)
	{
		dealCard(card);
	}

	public Card removeCard()
	{
		if (isEmpty())
			return null;
		else if (orderList.getLast() == 1)
			orderList.removeLast();
		else
		{
			int i = orderList.removeLast();
			i--; orderList.add(i);
		}
		return list.removeLast();
	}
	
	public boolean moveFrom(Cell other)
	{
		if (other.isEmpty() || other instanceof CellHome)
			return false;
		else
		{
			if (isEmpty())
			{
				if (other instanceof CellDraw)
				{
					if (other.getTop().getRank() == 13)
					{
						addCard(other.removeCard());
						return true;
					}
				}
				else if (other instanceof Tableau)
				{
					int otherTopInOrder = ((Tableau)other).topInOrder();
					if (otherTopInOrder == 1)
					{
						if (other.getTop().getRank() == 13)
						{
							addCard(other.removeCard());
							return true;
						}
					}
					else
					{
						LinkedList<Card> tmplst = new LinkedList<Card>();
						for (int i=1; i<=otherTopInOrder; i++)
							tmplst.add(other.removeCard());
						if (tmplst.getLast().getRank() == 13)
						{
							while (! tmplst.isEmpty())
								addCard(tmplst.removeLast());
							return true;
						}
						else
						{
							while (! tmplst.isEmpty())
								((Tableau)other).dealCard(tmplst.removeLast());
						}
					}
				}
				return false;
			}
			else
			{
				int desiredRank = getTop().getRank()-1;
				String desiredColor;
				if (getTop().getColor() == "black") desiredColor = "red";
				else desiredColor = "black";
				if (other instanceof CellDraw)
					if (other.getTop().getRank() == desiredRank &&
						other.getTop().getColor().equals(desiredColor))
					{
						addCard(other.removeCard());
						return true;
					}
					else return false;
				else if (other instanceof Tableau)
				{
					int otherTopInOrder = ((Tableau)other).topInOrder();
					int otherTopRank = other.getTop().getRank();
					String otherTopColor = other.getTop().getColor();
					int rankDiff = desiredRank-otherTopRank;
					if (otherTopInOrder > rankDiff)
						if ((rankDiff%2 == 1 && ! otherTopColor.equals(desiredColor))
						  ||(rankDiff%2 == 0 && otherTopColor.equals(desiredColor)))
						{
							LinkedList<Card> tmplst = new LinkedList<Card>();
							for (int i=0; i<=rankDiff; i++)
								tmplst.add(other.removeCard());
							while (! tmplst.isEmpty())
								addCard(tmplst.removeLast());
							return true;
						}
				}
				return false;
			}
		}
	}
	
	public void clear()
	{
		super.clear();
		orderList.clear();
	}
	
	public boolean isInOrder()
	{
		return (orderList.size() == 1);
	}
	
	public int topInOrder()
	{
		return orderList.getLast();
	}
}
