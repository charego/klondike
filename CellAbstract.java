import java.util.Iterator;
import java.util.LinkedList;

public abstract class CellAbstract implements Cell
{	
	protected LinkedList<Card> list;

	public CellAbstract()
	{
		list = new LinkedList<Card>();
	}

	public void addCard(Card card)
	{
		list.add(card);
	}
	
	public Card removeCard()
	{
		if (! isEmpty())
			return list.removeLast();
		return null;
	}
	
	public Card getTop()
	{
		if (! list.isEmpty())
			return list.getLast();
		return null;
	}
	
	public int size()
	{
		return list.size();
	}
	
	public void clear()
	{
		list.clear();
	}

	public String toString()
	{
		String toString = "";
		for (Card c: list)
		{
			if (c.isFaceUp())
				toString += c.toString() + " ";
			else toString += "__ ";
		}
		return toString;
	}

	public boolean isEmpty()
	{
		return list.isEmpty();
	}
	
	public boolean isInOrder()
	{
		return false;
	}

	public Iterator<Card> iterator()
	{
		return list.iterator();
	}
}
