import java.awt.Color;
import java.awt.Graphics;
import javax.swing.Icon;

@SuppressWarnings("serial")
public class PanelHome extends PanelCell
{
	
	public PanelHome(Cell c, ViewInformer v)
	{
		super(c, v);
	}

    public void paintComponent(Graphics g)
    {
    	super.paintComponent(g);
    	Icon image;
    	if (! cell.isEmpty())
    	{
    		image = cell.getTop().getImage();
    		int x = (getWidth() - image.getIconWidth()) / 2;
    		int y = (getHeight() - image.getIconHeight()) / 2;
    		image.paintIcon(this, g, x, y);
    	}
    	else
    	{    		
    		image = Card.getBack();
    		g.setColor(Color.white);
    		int x = (getWidth() - image.getIconWidth()) / 2;
    		int y = (getHeight() - image.getIconHeight()) / 2;
    		g.drawRect(x, y, image.getIconWidth(), image.getIconHeight());
    	}
    }
}