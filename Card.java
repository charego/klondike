import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * Represents a playing card with a suit, rank, image, and face up status.
 */
public class Card implements Comparable<Card>
{
    private Suit suit;
    private String color;
    private int rank;
    private boolean faceUp;
    private Icon image;
    private static Icon CARD_BACK;

    /**
     * Constructor.
     * @param suit
     * @param rank
     */
    public Card(Suit suit, int rank)
    {
    	this.suit = suit;
    	this.rank = rank;
    	this.color = suit.getColor();
    	faceUp = false;
    	image = getImageFromFile(suit, rank);
    	if (CARD_BACK == null)
    		CARD_BACK = getBackFromFile();
    }

    /**
     * Returns the card's face image if its face is up or its back side image otherwise.
     * @return the card's face image or the back side image
     */
    public Icon getImage()
    {
    	if (faceUp)
    		return image;
    	else
    		return CARD_BACK;
    }

    /**
     * Returns the back side image of a card.
     * @return the back side image of a card
     */
    public static Icon getBack()
    {
    	if (CARD_BACK == null)
    		new Card(Suit.spade, 1);
    	return CARD_BACK;
    }

    /**
     * Turns the card over, negating its face up status.
     */
    public void turn()
    {
    	faceUp = ! faceUp;
    }

    private Icon getImageFromFile(Suit suit, int rank)
    {
    	String fileName = "DECK/";
    	fileName += rank;
    	fileName += Character.toUpperCase(suit.toString().charAt(0));
    	fileName += ".png";
    	return new ImageIcon(getClass().getResource(fileName));
    }

    private Icon getBackFromFile()
    {
    	String fileName = "DECK/CARDBACK.png";
    	return new ImageIcon(getClass().getResource(fileName));
    }

    /**
     * Returns the card's face up status.
     * @return true if face up or false otherwise
     */
    public boolean isFaceUp()
    {
       return faceUp;
    }

    /**
     * Returns the card's suit.
     * @return the card's suit
     */
    public Suit getSuit()
    {
        return suit;
    }

    /**
     * Returns the card's rank.
     * @return the card's rank
     */
    public int getRank()
    {
        return rank;
    }
    
    /**
     * Returns the card's color.
     * @return the card's color
     */
    public String getColor(){
    	return color;
    }

    /**
     * Compares two cards with respect to rank
     * @return 0 if equal, < 0 if less, > 0 if greater
     */
    public int compareTo(Card other){
        return this.rank - other.rank;
    }

    /**
     * Returns the string representation of the card (<rank> of <suit>)
     * @return the string representation of the card
     */
    public String toString(){
        return rankToString(rank) + suit.toString().charAt(0);
    }

    static private String rankToString(int rank){
        if (rank >= 2 && rank <= 9) return rank + "";
        else if (rank == 10) return "T";
        else if (rank == 11) return "J";
        else if (rank == 12) return "Q";
        else if (rank == 13) return "K";
        else return "A";
    }
}
