import java.util.Iterator;

/**
 * Cell interface.
 */
public interface Cell extends Iterable<Card>
{
	/**
	 * Adds a card to the top of the cell.
	 * @param card the card to be added.
	 */
	public void addCard(Card card);
	
	/**
	 * Removes the top card from the cell.
	 * @return the top card in the cell.
	 */
	public Card removeCard();
	
	/**
	 * Moves contents from another cell to this cell, if the move is valid.
	 * @param cell the cell from which contents may be moved.
	 * @return true if the moved is successful.
	 */
	public boolean moveFrom(Cell other);
	
	/**
	 * Returns the top card in the cell, if a card is present.
	 * @return the top card in the cell, otherwise null.
	 */
	public Card getTop();

	/**
	 * Returns the number of cards in the cell.
	 * @return the number of cards in the cell.
	 */
	public int size();

	/**
	 * Clears the contents of the cell.
	 */
	public void clear();

	/**
	 * Returns true if the contents of the cell are in order.
	 * @return true if the contents of the cell are in order.
	 */
	public boolean isInOrder();

	/**
	 * Returns a string representation of the cell.
	 * @return a string representation of the cell.
	 */
	public String toString();

	/**
	 * Returns true if the cell is empty.
	 * @return true if the cell is empty.
	 */
	public boolean isEmpty();

	/**
	 * Returns an iterator for the cell.
	 * @return an iterator for the cell.
	 */
	public Iterator<Card> iterator();
}
