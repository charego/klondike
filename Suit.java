public class Suit implements Comparable<Suit>
{
	static public final Suit spade   = new Suit(4);
    static public final Suit heart   = new Suit(3);   
    static public final Suit diamond = new Suit(2);
    static public final Suit club    = new Suit(1);
    private int order;
    private String name;
    private String color;

    Suit(int ord)
    {
        order = ord;
        if (order == 1)
        {
        	name = "clubs";
        	color = "black";
        }
        else if (order == 2)
        {
        	name = "diamonds";
        	color = "red";
        }
        else if (order == 3)
        {
        	name = "hearts";
        	color = "red";
        }
        else if (order == 4)
        {
        	name = "spades";
        	color = "black";
        }
    }

    public int compareTo(Suit other)
    {
        return order - other.order;
    }

    public String toString()
    {
        return name;
    }
 
    public String getColor()
    {
    	return color;
    }
}