public class CellDraw extends CellAbstract
{
	private int indexOfTop;
	
	public CellDraw()
	{
		indexOfTop = 0;
	}
	
	public void addCard(Card card)
	{
		super.addCard(card);
		if (indexOfTop != 0)
			indexOfTop++;
	}
	
	public Card getTop()
	{
		return list.get(indexOfTop);
	}
	
	public Card removeCard()
	{
		Card card = list.remove(indexOfTop);
		indexOfTop--;
		if (indexOfTop < 0)
			indexOfTop = size()-1;
		return card;
	}
	
	public void updateDisplay()
	{
		if (! isEmpty())
		{
			indexOfTop++;
			if (indexOfTop >= size())
				indexOfTop = 0;
		}
	}

	public void clear()
	{
		super.clear();
		indexOfTop = 0;
	}
	
	public boolean moveFrom(Cell other)
	{
		return false;
	}
}