public class CellHome extends CellAbstract
{
	public CellHome() {}

	public boolean moveFrom(Cell other)
	{
		if (! other.isEmpty() && ! (other instanceof CellHome))
			if (list.isEmpty()) {
				if (other.getTop().getRank() == 1)
				{
					addCard(other.removeCard());
					return true;
				}
				return false;
			}
			else if	((other.getTop().getRank() == getTop().getRank()+1) &&
				other.getTop().getSuit().compareTo(getTop().getSuit())==0)
			{
				addCard(other.removeCard());
				return true;
			}
		return false;
	}

	public boolean isInOrder()
	{
		return true;
	}
}
