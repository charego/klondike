import java.awt.Color;
import java.awt.Graphics;
import javax.swing.Icon;

@SuppressWarnings("serial")
public class PanelTableau extends PanelCell
{
	public PanelTableau(Cell c, ViewInformer v)
	{
		super(c, v);
	}

    public void paintComponent(Graphics g)
    {
    	super.paintComponent(g);
    	Icon image;
    	int x;
    	int y = 50;
    	if (! cell.isEmpty())
    	{
    		for (Card c: cell)
    		{
    			image = c.getImage();
    			x = (getWidth() - image.getIconWidth()) / 2;
    			image.paintIcon(this, g, x, y);
    			y += 22;
    		}
    	}
    	else {
    		image = Card.getBack();
    		g.setColor(Color.yellow);
    		x = (getWidth() - image.getIconWidth()) / 2;
    		g.drawRect(x, y, image.getIconWidth(), image.getIconHeight());
    	}
    	if (isSelected)
    	{
    		// Highlight the cards that are ordered, if there are cards.
    		if (! cell.isEmpty())
    		{
	    		if (y > 50)
	    			y -= 22;
	    		int multiplier = ((Tableau) cell).topInOrder();
	    		int heightOfRect = (multiplier-1)*22;
	    		x = (getWidth() - cardWidth) / 2;
	    		g.setColor(highlight);
	    		g.fillRect(x, y-heightOfRect, cardWidth,
	    				heightOfRect+cardHeight);
    		}
    		else {
    			// Highlight the empty cell.
        		g.setColor(highlight);
        		x = (getWidth() - cardWidth) / 2;
        		g.fillRect(x, y, cardWidth, cardHeight);
    		}
    	}
    }
}