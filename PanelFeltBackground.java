import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PanelFeltBackground extends JPanel
{	
	private Image img;
	
	public PanelFeltBackground()
	{
		img = new ImageIcon(getClass().getResource("DECK/FELT.jpg")).getImage();
		Dimension d = new Dimension(1000, 800);
		setPreferredSize(d);
		setMinimumSize(d);
		setMaximumSize(d);
		setSize(d);
		setLayout(new BorderLayout());
	}

	public void paintComponent(Graphics g)
	{
		g.drawImage(img, 0, 0, null);
	}
}
